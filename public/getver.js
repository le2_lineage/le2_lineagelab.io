// ATTENTION
// REALLY HORRIBLE CODE DOWN THERE
// MOST OF IT IS COPY-PASTE FROM STACKOVERFLOW THO
// I HAVE NO IDEA HOW TO WRITE IN JS
// DONT THROW STUFF AT ME SAYING I AM RETARDED, MMKAY?
if (location.protocol != 'https:')
{
  location.href = 'https:' + window.location.href.substring(window.location.protocol.length);
}
var getJSON = function(url, callback) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', url, true);
    xhr.responseType = 'json';
    xhr.onload = function() {
      var status = xhr.status;
      if (status === 200) {
        callback(null, xhr.response);
      } else {
        callback(status, xhr.response);
      }
    };
    xhr.send();
};
function formatBytes(a,b){if(0==a)return"0 Bytes";var c=1024,d=b||2,e=["Bytes","KB","MB","GB","TB","PB","EB","ZB","YB"],f=Math.floor(Math.log(a)/Math.log(c));return parseFloat((a/Math.pow(c,f)).toFixed(d))+" "+e[f]}
function toDateTime(secs) {
    var t = new Date(1970, 0, 1); // Epoch
    t.setSeconds(secs);
    return t;
}
function formatDate(seconds) {
  var date = toDateTime(seconds);
  var monthNames = [
    "January", "February", "March",
    "April", "May", "June", "July",
    "August", "September", "October",
    "November", "December"
  ];

  var day = date.getDate();
  var monthIndex = date.getMonth();
  var year = date.getFullYear();

  return day + ' ' + monthNames[monthIndex] + ' ' + year;
}
var write_updatelist = function(error, response) {
  console.log("Reading OTA list...");
  var latest = {"datetime":0};
  for (var update in response["response"]) {
    console.log("Adding", response["response"][update]["id"])
    document.getElementById("otalist").innerHTML += "<li class=\"list-group-item\"><div class=\"ota d-flex flex-row justify-content-between\" id=\"item-" + response["response"][update]["id"] + "\"><div id=\"info-" + response["response"][update]["id"] + "\">Lineage " + response["response"][update]["version"] + " " + response["response"][update]["romtype"] +"</div></div></li>";
    document.getElementById("info-" + response["response"][update]["id"]).innerHTML += "<br>" + formatDate(response["response"][update]["datetime"]) + ", " + formatBytes(response["response"][update]["size"]);
    document.getElementById("item-" + response["response"][update]["id"]).innerHTML += "<a href=\"" + response["response"][update]["url"] + "\"type=\"button\" class=\"btn btn-default\"><i class=\"fa fa-download ml-1\"></i>Download</a>"
    if (response["response"][update]["datetime"] > latest["datetime"]) {
      latest = response["response"][update]
    }
  }
  document.getElementById("download-latest").href = latest["url"]
  document.getElementById("download-latest").append(" (" + formatDate(latest["datetime"]) + ")")
}
console.log("Downloading OTA list...")
getJSON("https://server.octonezd.me/ota/s2.json", write_updatelist);
